## Steps to Running The Program File

Edit this to how the code is correctly compiled

1. Save the Contents of the File to your computer, making sure everything remains in the subfile relative to each other
2. Update the path where the file is saved to the "Test_Data.txt"
3. Run the main file
4. Update the path where the file is saved to the "minitestdata.txt"
5. Run the main file

Note: There are two document files. One is the original file. The second is labelled "minitestdata". 
After creating the B+ tree, we had problems with the split algorithm.
While Nick was debugging the core B+ tree class, Kat used this file to be able to create a GUI and test the Add, Modify, 
and Delete Part functions.
Test with both documents to see all functions.