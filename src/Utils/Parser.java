package Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Parser {
	
	private File fileToParse 			= null;
	private BufferedReader br 			= null;
	private Map<String, String> dataMap = null;
	
	public Parser(String path){
		// Create file object
		fileToParse = new File(path);
		
		// Create buffered reader object for the file
		try {
			br = new BufferedReader(new FileReader(fileToParse));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String, String> parse(){
		dataMap = new HashMap<String, String>();
		
		try {
			String line = "";
			while ((line = br.readLine()) != null){
				String[] tokens = line.split("\\s{2,}");
				dataMap.put(tokens[0], tokens[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return dataMap;
	}
	
	public void printMap(){
		if (this.dataMap != null){
			for (Map.Entry<String,String> entry : dataMap.entrySet()){
				System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			}
		}
	}
    

}
