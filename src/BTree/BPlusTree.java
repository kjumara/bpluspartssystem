package BTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


//this allows us to use the Key and Value set up, allowing us to have keys AND a value as the record in the leaf node
public class BPlusTree <Key extends Comparable<? super Key>, Value>{
	
    private static final int MIN_NODE_CAP 		= 2;
    private static final int MAX_INTERNAL_NODE_CAP 	= 4;
    private static final int MAX_LEAF_NODE_CAP 		= 16;

    //this creates a pointer to the root node
    private Node root;
    
    //creates variables to keep track of statistics
    private int splits, parentSplits, fusions, parentFusions, currentDepth, treeDepth;
    
    //constructor class defining the root
    BPlusTree (){
        root = new LeafNode(); 
        splits = 0;
        parentSplits = 0;
        fusions = 0;
        parentFusions = 0;
        treeDepth = 0;
    }
    
    //recursive classes starting at root
    
    public int getTreeDepth(){
        return treeDepth;
    }
    
    public Value Search(Key key){
        return root.getRecord(key);
    }
    
    public void insert(Key key, Value value){
        currentDepth = 0;
        root.insert(key, value);
    }
   
    public void delete(Key key){
        root.delete(key);
    }

    //creates a node class that can be extended into internal and leaf nodes
    private abstract class Node{
        ArrayList<Key> keys;
        
        //number of keys within a node
        int keynum(){return keys.size();}
        
        //basic methods
        abstract Value getRecord(Key key);
        abstract void delete(Key key);       
        abstract void insert(Key key, Value value);
        abstract void merge(Node sibling);
        abstract Node split();
        
        //allows adding new node easier
        abstract Key getFirstLeafKey();
     
        //makes code cleaner and checks if Split/Merge is needed
        abstract boolean isOverflow();
        abstract boolean isUnderflow();
    }
    
    //class for InternalNode's which works upon the Node Class
    private class InternalNode extends Node{
        ArrayList <Node> children;
        
        //constructor method for new node
        InternalNode() {
            this.keys = new ArrayList();
            this.children = new ArrayList();
        }

        @Override
        Value getRecord(Key key) { return getChild(key).getRecord(key);}
        
        // Method to find a child at a given key
        Node getChild(Key key) {
            int childIndex = Collections.binarySearch(keys, key);
            // If the key already exists in the node, grab the keyIndex + 1 child since there is an offset
            if(childIndex >= 0){
            	childIndex++;
            }
            // If binSearch returns negative value, element is not found
            else if (childIndex < 0){
            	childIndex = Math.abs(childIndex+1);
            }
            return children.get(childIndex);
        }
        
        Node getChildLeftSibling(Key key) {return null;}
        Node getChildRightSibling(Key key) {return null;}

        @Override
        void delete(Key key) {
            Node child = getChild(key);
            child.delete(key);
            if (child.isUnderflow()){
                incrementParentFusions();
                Node childLeftSibling = getChildLeftSibling(key);
                childLeftSibling.merge(child);
                if (childLeftSibling.isOverflow()){
                    incrementSplits();
                    Node sibling = childLeftSibling.split();
                    insertChild(sibling, sibling.getFirstLeafKey());
                }
            }
        }
        
        @Override
        Key getFirstLeafKey(){
            return children.get(0).getFirstLeafKey();
        }
        
        void insertChild(Node newChild, Key key){
        	int childIndex = Collections.binarySearch(keys, key);
            // If the key already exists in the node, grab the keyIndex + 1 child since there is an offset
            if(childIndex >= 0){
            	childIndex++;
            	children.set(childIndex, newChild);
            }
            // If binSearch returns negative value, element is not found
            else if (childIndex < 0){
            	childIndex = Math.abs(childIndex+1);
            	if (childIndex > keys.size()){
            		keys.add(key);
            	}
            	if (childIndex > children.size()){
            		children.add(newChild);
            	}
            	else{
            		keys.add(childIndex, key);
            		children.add(childIndex, newChild);
            	}
            }
        }
        

        @Override
        void insert(Key key, Value value) {
            incrementDepth();
            if (currentDepth > treeDepth){setTreeDepth();}
            Node child = getChild(key);
            child.insert(key, value);
            if(child.isOverflow()){
            	// Must split the CHILD not the current internal node
                Node sibling = child.split();
                insertChild(sibling, sibling.getFirstLeafKey());
                incrementSplits();
            }
            if (root.isOverflow()){
                incrementParentSplits();
                Node sibling = split();
                InternalNode newRoot = new InternalNode();
                newRoot.keys.add(sibling.getFirstLeafKey());
                newRoot.children.add(this);
                newRoot.children.add(sibling);
                root = newRoot;
            }
        }

        @Override
        void merge(Node sibling) {
            incrementFusions();
            InternalNode node = (InternalNode) sibling;
            keys.addAll(node.keys);
            children.addAll(node.children);
        }

        @Override
        Node split() {
        	// Create new InternalNode of same size
        	InternalNode sibling = new InternalNode();
        	
        	// Move second half of the data from first InternalNode into sibling InternalNode
            int start = keynum()/2+1;
            int end = keynum();
            
            // Copy keys over to new node and delete from old
            List<Key> keysToMove = keys.subList(start, end);
            sibling.keys.addAll(keysToMove);
            keys.removeAll(keys.subList(start-1, end));
            
            // Copy values over to new node and delete from old
            List<Node> childrenToMove = children.subList(start, end + 1);
            sibling.children.addAll(childrenToMove);
            children.removeAll(childrenToMove);
            
            
            
            return sibling;
        }

        @Override
        boolean isOverflow() {return keynum() > MAX_INTERNAL_NODE_CAP;}

        @Override
        boolean isUnderflow() {return keynum() < MIN_NODE_CAP;}

    }
    
    //class for leafNode's which work upon the Node Class
    private class LeafNode extends Node{

        ArrayList <Value> records;
        LeafNode next;
        
        //constructor class
        LeafNode (){
            keys = new ArrayList();
            records = new ArrayList();
        }
        
        //TO DO: CREATE A LOG ERROR FOR IF LOCATION IS NEGATIVE
        @Override
        Value getRecord(Key key) {
            int recordIndex = Collections.binarySearch(keys, key);
            if (recordIndex >= 0)
                return records.get(recordIndex);
            else 
                return null;
        }

        @Override
        void delete(Key key) {
            int index = Collections.binarySearch(keys, key);
            if (index >= 0){
                keys.remove(index);
                records.remove(index);
            }
        }

        @Override
        void insert(Key key, Value value) {
        	// If other keys are present we must find where this one belongs in the node
            if(keys.size()>0){
                int index = Collections.binarySearch(keys, key);
                // If the key already exists in the node just set the value
                if(index >= 0){
                	records.set(index, value);
                }
                // If binSearch returns negative value, element is not found. We must add key and value
                // It returns (-indexOfWhereItShouldBe-1) so insert at |-indexOfWhereItShouldBe|
                else if (index < 0){
                	index = Math.abs(index+1);
                	if (index > keys.size()){
                		keys.add(key);
                	}
                	if (index > records.size()){
                    	records.add(value);
                	}
                	else{
                		keys.add(index, key);
                    	records.add(index, value);
                	}
                }
            }
            // This is the first key to be inserted. Index does not matter
            else{
                keys.add(key);
                records.add(value);
            }
            // Check if we have exceeded capacity
            if (root.isOverflow()){
                incrementParentSplits();
            	// Split this leaf into two
                Node sibling = split();
                InternalNode newRoot = new InternalNode();
                
                // Copy key up from sibling node to root node
                newRoot.keys.add(sibling.getFirstLeafKey());
                
                // Add this leaf and its sibling as children of the new root
                newRoot.children.add(this);
                newRoot.children.add(sibling);
                root = newRoot;   
            }
        }

        @Override
        void merge(Node sibling) {
            LeafNode siblingNode = (LeafNode) sibling;
            keys.addAll(siblingNode.keys);
            records.addAll(siblingNode.records);
            next = siblingNode.next;
        }

        @Override
        Node split() {
        	// Create new leafNode of same size
            LeafNode sibling = new LeafNode();
            
            // Move second half of the data from first leafNode into sibling leafNode
            int start = (keynum()/2)+1;
            int end = keynum();
            
            // Copy keys over to new node and delete from old
            List<Key> keysToMove = keys.subList(start, end);
            sibling.keys.addAll(keysToMove);
            keys.removeAll(keysToMove);
            
            // Copy values over to new node and delete from old
            List<Value> valuesToMove = records.subList(start, end);
            sibling.records.addAll(valuesToMove);
            records.removeAll(valuesToMove);
            
            //TODO: Set node pointers. Leaves must be linked list
             
            return sibling;
            
        }

        @Override
        boolean isOverflow() {return records.size() > MAX_LEAF_NODE_CAP;}

        @Override
        boolean isUnderflow() {
            if(!this.equals(root)) 
                return records.size() < MIN_NODE_CAP;
            else
                return true;
        }

        @Override
        Key getFirstLeafKey() {return keys.get(0);}
        
    }

     //updates statistic variables
    private void incrementSplits(){
       splits+=1;
    }
    
    private void incrementParentSplits(){
       parentSplits+=1;
    }
    
    private void incrementFusions(){
        fusions+= 1;
    }
    
    private void incrementParentFusions(){
        parentFusions+= 1;
    }
    
    private void incrementDepth(){
        currentDepth += 1;
    }
    
    private void setTreeDepth(){
        treeDepth = currentDepth;
    }
    
      //returns statistic variables
    public int getSplits(){
        return splits;
    }
    
    public int getParentSplits(){
        return parentSplits;
    }
    
    public int getFusions(){
        return fusions;
    }
    
    public int getParentFusions(){
        return parentFusions;
    }
    
  /*TODO: ALL CLASSES ARE BEING SUBSUMED WITHIN INTERNAL AND LEAF NODES
    //recursive searching method searching a node and then repeating lower in the tree
    private void searchTree(Node N, String record){
        
    }
    
    //method used to begin the recursive searching algorithm
    public void searchTree(String record){
        //searching algorithm begins at root node
        searchTree(root,record);
    }
    
    //splitting recursive algorithm called within the insert function
    private void split(Node N, String record){
    }
    
    //recursive insertion method starting from the root and repeating lower in the tree
    private void insert(Node N, String record){
        //if else where it inserts and if necessary it calls split
    }
    
    //method used to begin the recursive insertion algorithm
    public void insert(String record){
        //calls the recursive method beginning at the root node
        insert(root, record);
    }
    
    //merging recursive algorithm called within the delete function
    private void merge(Node First,  Node Second){
        
    }
    
    //recursive deletion algorithm
    private void delete(Node N, String record){
        //if else where it deletes and merges if necessary
    }
    
    //method used to begin the recursive deletion 
    public void delete(String record){
        //calls the recursive method beginning at root node
        delete(root, record);
    }
    */
}
