package BTree;

import java.util.Map;
import java.io.FileNotFoundException;
import Utils.Parser;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

// Driver class for project
public class Main {
	public static void main(String[] args) {
        BPlusTree <String, String> tree = new BPlusTree <String, String>();
        Parser p = new Parser("Documents/test_data.txt");
        Map<String, String> map = p.parse();

        if (map != null){
            for (Map.Entry<String,String> entry : map.entrySet()){
                String key = entry.getKey().replace("-", "");
                tree.insert(key, entry.getValue());
            }
        }
        
       Scanner scan = new Scanner(System.in);
       boolean exit = false;
       while(!exit){
          displayMenu();
          int choice = scan.nextInt();
          
          switch (choice){
              case 1: //query part
                  String queriedRecord = tree.Search(findKey(scan));
                  if (queriedRecord == null)
                     System.out.println("Error: Part Not Found.");
                  else
                      System.out.println("Part Description:\t" + queriedRecord);
                  
                  break;
                  
              case 2: //display next 10 parts
                  break;
              case 3: //modify part description
                   String oldKey = findKey(scan);
                   if (tree.Search(oldKey) == null){
                     System.out.println("Error: Part Does not Exist.\nIf necessary, select 4 to Add Part.\n");
                  }
                  else{
                      System.out.println("Input New Record: ");
                      String newRecord = scan.nextLine();
                      newRecord = scan.nextLine();
                      tree.delete(oldKey);
                      tree.insert(oldKey, newRecord);
                      System.out.println("Item \t" + oldKey + "\t has been updated to:\t" + newRecord);
                  }
                  break;
              case 4: //add part
                  String newKey = findKey(scan);
                  if (tree.Search(newKey) == null){
                      System.out.println("Input Record: ");
                      String newRecord = scan.nextLine();
                      newRecord = scan.nextLine();
                      tree.insert(newKey, newRecord);
                      System.out.println("Item \t" + newKey + ":\t" + newRecord + " has been added");
                  }
                  else{
                      System.out.println("Error: Part Already Exists.\nIf necessary, select 3 to Modify Part\nOr select new key");
                  }
                  break;
              case 5: //delete part
                  String deletedKey = findKey(scan);
                  if(tree.Search(deletedKey)==null){
                      System.out.println("Error: Part Does Not Exist.");
                  }
                  else{
                      tree.delete(deletedKey);
                      System.out.println("Part Successfully Deleted");
                  }
                  break;
                  
              case 6: //exit menu
                  exit = true;
                  break;
          }
          
       }
       
       System.out.println("Program Ended.");
       displayStatistics(tree);
       System.out.println("Enter 'y' or 'Y' if you would like to save the data to a new file.");
       String dataChoice = scan.next();
       if (dataChoice.equalsIgnoreCase("y"))
           newFile(scan, tree);
    
    }
    
    public static void displayMenu(){
        System.out.println("\tOptions ");
        System.out.println("1: \tQuery Part");
        System.out.println("2: \tDisplay Next 10 Parts");
        System.out.println("3: \tModify Part Description");
        System.out.println("4: \tAdd Part");
        System.out.println("5: \tDelete Part");
        System.out.println("6: \tExit");
    }
    
    public static String findKey (Scanner scan){
        System.out.println("Input Part Key: ");
        String strKey = scan.next();
        return strKey.replace("-", "");
    }
    
    public static void displayStatistics(BPlusTree tree){
        //print number of splits, parent splits, fusions, parent fusions, and tree depth
        System.out.println("Splits:\t"+tree.getSplits());
        System.out.println("Parent Splits:\t"+tree.getParentSplits());
        System.out.println("Fusions:\t"+tree.getFusions());
        System.out.println("Parent Fusions:\t"+tree.getParentFusions());
        System.out.println("Tree Depth:\t" + tree.getTreeDepth());
    }
    
    public static void newFile(Scanner scan, BPlusTree tree){
        System.out.println("Input file name: ");
        String fileName = scan.next();
        
        try{
            File newTree = new File(fileName + ".txt");
            if (newTree.createNewFile()){
                System.out.println("File Created: " + newTree.getName());
                if(newTree.canWrite()){
                    FileWriter writeTree = new FileWriter(newTree.getName());
                    
                    //CREATE A LOOP WHERE IT WRITES ALL OF THE B+ TREE ONE BY ONE
                }
            
            }
            else{
                System.out.println("File Already Exists");
            }
        }catch (IOException e){
            System.out.println("An Error Has Ocurred");
        }
        
    }
    
}
