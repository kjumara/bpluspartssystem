
import java.util.Map;
import java.io.FileNotFoundException;
import Utils.Parser;

// Test class for parser (TESTING PURPOSES ONLY)
public class ParserTest{

    /**
     *
     * @param args
     * @throws FileNotFoundException
     */
    
    public static void main(String[] args) throws FileNotFoundException{
        Parser p = new Parser("Documents/test_data.txt");
        Map<String, String> map = p.parse();
        p.printMap();
        
        
    }

}
